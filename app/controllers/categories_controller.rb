class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :redirect_to_login_path

  def index
    @categories = Category.all
    flash.now[:info] = "まだカテゴリが登録されていません" if @categories.empty?
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "登録しました！"
      redirect_to @category
    else
      flash.now[:danger] = "しっぱいしました"
      render 'new'
    end
  end

  def edit
  end

  def show
    @posts = @category.posts.order(updated_at: :desc).page(params[:page])
  end

  def update
    if @category.update(category_params)
      flash[:success] = "更新しました！"
      redirect_to @category
    else
      flash.now[:danger] = "しっぱいしました"
      render 'edit'
    end
  end

  def destroy
    if @category.destroy
      flash[:success] = "削除しました"
      redirect_to categories_path
    else
      flash.now[:danger] = "しっぱいしました"
      render 'show'
    end
  end

  private
    def category_params
      params.require(:category).permit(:name, :user_id)
    end

    def set_category
      @category = Category.find(params[:id])
    end
end
