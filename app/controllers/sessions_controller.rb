class SessionsController < ApplicationController
  def index
  end
  
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      log_in user
      flash[:success] = "ログインしました！"
      redirect_to user
    else
      flash.now[:danger] = "ログインにしっぱいしました"
      render 'new'
    end
  end

  def destroy
    log_out
    flash[:success] = "ログアウトしました"
    redirect_to login_path
  end
end
