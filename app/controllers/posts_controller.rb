class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :redirect_to_login_path

  def index
    @posts = Post.where(user_id: current_user.id).includes(:category).order(updated_at: :desc).page(params[:page])
    flash.now[:info] = "まだ投稿がありません" if @posts.empty?
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:success] = "登録しました！"
      redirect_to @post
    else
      flash.now[:danger] = "しっぱいしました"
      render 'new'
    end
  end

  def edit
  end

  def show
  end

  def update
    if @post.update(post_params)
      flash[:success] = "更新しました！"
      redirect_to @post
    else
      flash.now[:danger] = "しっぱいしました"
      render 'edit'
    end
  end

  def destroy
    if @post.destroy
      flash[:success] = "削除しました"
      redirect_to posts_path
    else
      flash.now[:danger] = "しっぱいしました"
      render 'show'
    end
  end

  private
    def post_params
      params.require(:post).permit(:title, :content, :user_id, :category_id)
    end

    def set_post
      @post = Post.find(params[:id])
    end
end
