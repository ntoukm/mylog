# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  email           :string(255)
#  password_digest :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ActiveRecord::Base
  has_secure_password

  has_many :categories, dependent: :delete_all
  has_many :posts, dependent: :delete_all

  validates :name, presence: true, length: { minimum: 2 }
  validates :email, presence: true
  validates :password, presence: true, length: { minimum: 6 }
end
