# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ActiveRecord::Base
  belongs_to :user
  has_many :posts, dependent: :delete_all

  validates :name, presence: true, uniqueness: true

  # ログインユーザーに紐づくカテゴリ一覧を更新順に取得する
  def self.list_for(current_user)
    where(user_id: current_user.id).order(updated_at: :desc).map{ |category| [category.name, category.id] }
  end
end
