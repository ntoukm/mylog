# == Schema Information
#
# Table name: posts
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  content     :text(65535)
#  user_id     :integer
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Post < ActiveRecord::Base
  belongs_to :category
  belongs_to :user

  paginates_per 5

  def self.latest_post_time(category)
    order(updated_at: :desc).first.updated_at
  end
end
